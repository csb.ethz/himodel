# himodel: model of the hemagglutination inhibition assay
#
# Copyright (C) 2020 ETH Zurich, D-BSSE, Janina Linnik
#
# This file is part of himodel.
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

#' @title Hemagglutination of a single serum dilution
#'
#' @description Returns the degree of hemagglutination for a single serum
#'   dilution and specified input values. Wrapper function that consecutively
#'   executes \code{\link[himodel]{hemagglutinin_occupancy}},
#'   \code{\link[himodel]{virus_density_on_rbc}} and
#'   \code{\link[himodel]{hemagglutination_degree}}.
#'   Follows by default the WHO hemagglutination inhibition (HI) assay protocol
#'   for pandemic influenza A/California/7/2009 (H1N1).
#'
#' @export
#' @include hemagglutinin_occupancy.R
#' @include model_virus_rbc_binding.R
#' @include virus_density_on_rbc.R
#' @include hemagglutination_degree.R
#'
#' @param ab_dilution_conc Antibody concentration in diluted serum in unit nM.
#' @param K_d_app Apparent dissociation constant (avidity) of antibody-virus
#'   interaction in unit nM.
#' @param virus_conc Initial virus concentration in nM. Numeric value > 0.
#'   Default value corresponds to 4 HA units.
#' @param rbc_conc Initial concentration of red blood cells (RBCs) in nM.
#'   Numeric value > 0. Default value corresponds to 0.75\% (v/v) suspension.
#' @param t_readout Readout time in seconds. Default is 1800 s (= 30 min).
#' @param r Number of hemagglutinin receptors per virus, default is 400.
#' @param e Number of epitopes per hemagglutinin receptor, default is 3.
#' @param e_star Number of covered hemagglutinin epitopes by one bound antibody
#'   molecule. Default is 3.
#' @param k_ass_rbc Association rate constant of virus-RBC binding, default is
#'   2e-6 per second (1/s).
#' @param k_diss_rbc Dissociation rate constant of virus-RBC binding, default is
#'   2e-4 per nM per second (1/(nM  s)).
#' @param b Number of sialic acid linked receptors per RBC, default is 0.45*1e6.
#' @param b_star Number of covered sialic acid linked receptors by one bound
#'   virion, default is 34.
#' @param k_agg Kinetic rate constant of agglutination, default set to 2e6 per
#'   second (1/s).
#' @param t_steps Time steps for ODE solver, default is 1 second.
#' @param model ODE model of virus-RBC binding passed on to
#'   \code{\link[deSolve]{ode}}. Model is defined in
#'   \code{\link[himodel]{model_virus_rbc_binding}}.
#' @param return_scalar Boolean, default is FALSE. If TRUE, the degree of
#'   hemagglutination is returned as a numeric value. If FALSE, all results and
#'   input values are returned as a data frame.
#' @param ... Other arguments passed on to \code{\link[deSolve]{ode}}.
#'
#' @return A data frame containing all results and input values.
#'
#' @examples
#' ## For a single sample:
#' results <- simulate_hemagglutination(
#'   ab_dilution_conc = 5, # nM
#'   K_d_app = 10 # nM
#' )
#' results$hemagg_degree
#'
#' ## For multiple samples:
#' # Load library for functional programming
#' library(purrr)
#'
#' # Create data frame with sample-specific antibody concentrations and avidities
#' data = data.frame(ab_dilution_conc = c(10, 1, 5), K_d_app = c(10, 1, 0.5))
#'
#' # Iterate over all samples
#' results <- purrr::map_df(
#'   1:nrow(data),
#'   ~ simulate_hemagglutination(data$ab_dilution_conc[.], data$K_d_app[.])
#' )
#'
#' results[, 1:3]
simulate_hemagglutination <- function(ab_dilution_conc,
                                      K_d_app,
                                      virus_conc = 0.00013,
                                      rbc_conc = 3.114e-5,
                                      t_readout = 1800,
                                      r = 400,
                                      e = 3,
                                      e_star = 3,
                                      k_ass_rbc = 2e-6,
                                      k_diss_rbc = 2e-4,
                                      b = 0.45*1e6,
                                      b_star = 34,
                                      k_agg = 2e6,
                                      t_steps = 1,
                                      model = model_virus_rbc_binding,
                                      return_scalar = FALSE,
                                      ...)
{
  # TODO: Add message on valid RBC concentration range

  # Convert concentrations to final concentrations
  # First assay step: 25 uL serum dilution added to 25 uL virus dilution
  ab_conc_tot_1 = ab_dilution_conc / 2
  virus_conc_tot_1 = virus_conc / 2

  # Second assay step: 50 uL RBC are added to 50 uL serum/virus dilution
  virus_conc_tot_2 = virus_conc_tot_1 / 2
  rbc_conc_tot_2 = rbc_conc / 2

  # Model step 1: returns hemagglutinin occupancy theta
  theta <- hemagglutinin_occupancy(
    ab_conc_tot = ab_conc_tot_1,
    K_d_app = K_d_app,
    virus_conc_tot = virus_conc_tot_1,
    r = r,
    e = e,
    e_star = e_star
  )
  # Model step 2: returns virus density rho
  rho <- virus_density_on_rbc(
    theta = theta,
    virus_conc_tot = virus_conc_tot_2,
    rbc_conc_tot = rbc_conc_tot_2,
    t_readout = t_readout,
    r = r,
    k_ass_rbc = k_ass_rbc,
    k_diss_rbc = k_diss_rbc,
    b = b,
    b_star = b_star,
    t_steps = t_steps,
    model = model,
    ...
  )
  # Model step 3: returns degree of hemagglutination
  hemagg_degree <- hemagglutination_degree(
    theta = theta,
    rho = rho,
    rbc_conc_tot = rbc_conc_tot_2,
    t_readout = t_readout,
    k_agg = k_agg
  )

  if (return_scalar){
    out <- hemagg_degree
  }
  else {
    out <- data.frame(
      ab_dilution_conc = ab_dilution_conc,
      K_d_app = K_d_app,
      hemagg_degree = hemagg_degree,
      theta = theta,
      rho = rho,
      virus_conc = virus_conc,
      rbc_conc = rbc_conc,
      t_readout = t_readout,
      r = r,
      e = e,
      e_star = e_star,
      k_ass_rbc = k_ass_rbc,
      k_diss_rbc = k_diss_rbc,
      b = b,
      b_star = b_star,
      k_agg = k_agg,
      ab_conc_tot_1 = ab_conc_tot_1,
      virus_conc_tot_1 = virus_conc_tot_1,
      virus_conc_tot_2 = virus_conc_tot_2,
      rbc_conc_tot_2 = rbc_conc_tot_2
    )
  }
  out
}


