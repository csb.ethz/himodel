# himodel <img src="https://gitlab.com/csb.ethz/himodel/-/raw/master/inst/figure/sticker.png" align="right" width="150px"/>


[![Project Status](http://www.repostatus.org/badges/latest/active.svg)](http://www.repostatus.org/#active)
[![pipeline status](https://gitlab.com/csb.ethz/himodel/badges/master/pipeline.svg)](https://gitlab.com/csb.ethz/himodel/-/commits/master)
[![codecov](https://codecov.io/gl/csb.ethz/himodel/branch/%5Cx6d6173746572/graph/badge.svg)](https://codecov.io/gl/csb.ethz/himodel)

Simulate hemagglutination inhibition (HI) titers against influenza H1N1




## Introduction
This R package provides an implementation of a biophysical model of hemagglutination inhibition (HI) and a set of functions to simulate HI assays for influenza H1N1 [1]. This enables to predict the H1N1-specific HI titer from a given IgG antibody concentration and IgG antibody avidity.

For instance, for a given serum IgG concentration and apparent IgG avidity `K_d_app`, simply call
`simulate_hiassay` and then `predict_titer`:

```{r}
serum_antibody = 100 # nM
K_d_app = 1 # nM

assay_results <- simulate_hiassay(serum_antibody, K_d_app)
hi_titer <- predict_titer(assay_results)
```

See the documentation for more examples: https://csb.ethz.gitlab.io/himodel

The model and its application to infer antibody avidities from measured antibody concentrations and HI titers is described in detail in Linnik et al., 2021 (preprint available at https://www.biorxiv.org/content/10.1101/2020.10.05.326215v3) [2].

## Installation
If you do not have `devtools` installed, install it first:
```{r}
install.packages("devtools")
```
Then you can install the package directly from GitLab:
```{r}
devtools::install_git(url = "https://gitlab.com/csb.ethz/himodel", build_vignettes = TRUE)
```
And load it:
```{r}
library(himodel)
```

## Documentation
See https://csb.ethz.gitlab.io/himodel or load the package using
`library(himodel)` and call the vignette using `vignette("himodel")`.

## Author

* Janina Linnik <janina.linnik@bsse.ethz.ch>

## References
 [1] World Health Organization, "WHO manual on animal influenza diagnosis and surveillance", 2002.
  <br>
 [2] Linnik et al., "Model-based inference of neutralizing antibody avidities against influenza virus", 2021. <br>Preprint available: https://www.biorxiv.org/content/10.1101/2020.10.05.326215v3
 <br>
 [3] Dolgosheina et al., "A kinetic model of the agglutination process." 1992.
 <br>
 [4] De St. Groth, "The neutralization of viruses", 1963.
